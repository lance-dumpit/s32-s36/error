const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth"); 


// Route for checking if email exists
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));

});


// Routes for User Authentication
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Activity



// Route for Details
// The "auth.verify" will acts as a middleware to ensure that the user is looged in before they can get the details, if user is logged in
// after auth.very , after verification it only allow the req, res function
router.post("/details", auth.verify, (req,res) => {

	// decoded from token, object
	const userData = auth.decode(req.headers.authorization);
	// object, seen from github 
	console.log(userData);

	// userid the proerty
	// inside is the "confirm"
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

})


router.post("/enroll", auth.verify, (req,res) => {

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});


// Allows us to export the "router" obejct that will be accessed
module.exports = router;